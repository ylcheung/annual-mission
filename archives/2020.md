This page used to mark the Mission target annually. Also progress marked.

## Mission 1 ![](https://img.shields.io/badge/progress-100%25-green.svg)

Finish personal branding:

- [x] LinkedIn
- [x] [Personal Website](https://jackycyl.dev/)
- [x] Resume update
- [x] [gitbook pages update](https://gitbook.jackycyl.com/)
- [x] Personal Website enhancement with SSG - Gastby is used


## Mission 2 ![](https://img.shields.io/badge/progress-20%25-red.svg)

Finish 1st stage of projects, or find existing suitable one

- [ ] boilerplate
- [ ] fm101 (web service, cross user account gnucash)
- [x] Wikia (use barn as temp sol.)
- [ ] Toolkit: dev utility collection
- [ ] Gaming Idea

## Mission 3 ![](https://img.shields.io/badge/progress-0%25-red.svg)

Learn non-programming related things: 4

- [ ] First Aid
- [ ] Leather knitting
- [ ] A way to paint portrait
- [ ] Draw with Right Brain
- [ ] Project Management
- [ ] Vehicle Licence
- [ ] Combat skills

## Mission 4 ![](https://img.shields.io/badge/progress-200%25-green.svg)

Completion of games: 1 

- [ ] Dark Souls
- [ ] Dark Souls III
- [ ] Witcher 3
- [ ] Bloodborne
- [ ] The last of US
- [x] [Thronebreaker: The Witcher Tales](https://www.gog.com/game/thronebreaker_the_witcher_tales)
- [x] [Monster Hunter: World](https://monsterhunterworld.com)

## Mission 5 ![](https://img.shields.io/badge/progress-57%25-yellow.svg)

Learn programming-related applications: 

- [x] [Flyway](https://flywaydb.org/)
- [x] Message Queue: ActiveMQ / ~~RabbitMQ~~
- [x] Ansible / Puppet
- [x] HAProxy / Traefik
- [ ] Relational DB 101
- [x] [GastbyJS](https://www.gatsbyjs.org/)
- [ ] Java Spring

## Mission 6 ![](https://img.shields.io/badge/progress-75%25-yellow.svg)

Self-improvment:

- [x] Annual Body Check (20200522)
- [ ] Annual Dentist Visit
- [x] Laser Hair Removal (20200702)
- [x] Optional Laser Visual Modification LASIK (20200825)

### Reference

This page inspired from: [ResolutionBoard](https://github.com/sarthology/ResolutionBoard) for organize and tracking annual achievements.
