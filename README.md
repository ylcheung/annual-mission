This page used to mark the Mission target annually. Also progress marked.

# Annual Conclusion

## Solution finding ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] find a solution for easy search of (awesome) stuff

## Projects ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] Toolkit: dev utility collection
- [ ] Control center of microservice, authentication and authorization control
- [ ] fm101 -> webcash (web service, cross user account gnucash)
- [ ] Gaming Idea

## Knowledge ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] Network 101
- [ ] Relational DB 101
- [ ] Japanese

## Skills ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] Leather crafting
- [ ] Driving Licence (2022/07/22 written test)(Road Test?)
- [ ] Circuit
- [ ] First Aid
- [ ] A way to paint portrait - Draw with Right Brain
- [ ] Project Management Skill enhancement
- [ ] Combat Skill improvement

## Games ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] Hollow Knight: Silksong (202?)

## Personal ![](https://img.shields.io/badge/progress-0%25-red.svg)

- [ ] Annual Body Check 
- [ ] Annual Dentist Visit

# Reference

This page inspired from: [ResolutionBoard](https://github.com/sarthology/ResolutionBoard) for organize and tracking annual achievements.

